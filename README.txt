CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
Adds a new field mapper to the Salesforce suite of modules to allow 
using a term reference field. Entity references for content are 
supported natively by the Salesforce modules, but term references 
are not unfortunately.

REQUIREMENTS
------------

This module requires the following modules:

 * Entity API (https://www.drupal.org/project/entity)
 * Salesforce (https://www.drupal.org/project/salesforce)

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * drush en salesforce_term_ref -y

CONFIGURATION
-------------

Visit /admin/structure/salesforce/mappings to create a new mapping 
(/admin/structure/salesforce/mappings/add). Set the "Drupal Entity 
Type" to "Node" and then select the content type you want to target 
under "Drupal Entity Bundle." Under the "Field Map" section, you 
should be able to "Add another field mapping". From here you 
should see a new option called "Term Reference" under "Select Drupal 
field type." Select the term reference field from your content type.

MAINTAINERS
-----------

Current maintainers:
 * Jim Fisk (jimafisk) - https://www.drupal.org/u/jimafisk

This project has been sponsored by:
 * Jantcu
   Empowering organizations to take control of their technology.
   Visit http://jantcu.com for more information.
